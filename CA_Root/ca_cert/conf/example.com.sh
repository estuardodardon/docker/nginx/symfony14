export W_DIR=./ca_cert
export W_CERT_DIR=./ca_cert/files/certs/example.com
export W_CSR_DIR=./ca_cert/files/csr/example.com
export W_KEY_DIR=./ca_cert/files/keys/example.com
export W_EXT_DIR=./ca_cert/files/csr/example.com
export W_NOPASS_DIR=./ca_cert/files/nopass/example.com
export W_DB_DIR=./ca_cert/db/example.com
export W_CRL_DIR=./ca_cert/db/example.com/crl
export W_INDEX_DIR=./ca_cert/db/example.com/index
export W_CA_RAND=./ca_cert/db/example.com/.ca_rnd
export W_RND_FILE=./ca_cert/db/example.com/.rnd
export W_SERVER_DIR=./ca_cert/server/example.com
export W_CA_CERT=./ca_cert/server/example.com/ca_cert.pem
export W_CA_REQ=./ca_cert/server/example.com/ca_req.pem
export W_DH_FILE=./ca_cert/server/example.com/dh.crt
export W_CA_KEY=./ca_cert/server/example.com/ca_key.pem
export W_CNF=./ca_cert/conf/example.com.cnf
export W_ORGANIZACION='Example Cert'
export W_COMMON_NAME=example.com
export W_EMAIL=admin@example.com
export W_GLOBAL_SAN=*.example.com
export W_ALT_DNS=example.com
export W_REV_DNS=example.com.127.0.0.1
export W_DOM_IP=127.0.0.1
