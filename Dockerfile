# Symfony 1.4
#
# VERSION	1.0

# use the ubuntu base image provided by dotCloud
FROM ubuntu:22.04

# Environment variables
ENV ENVIRONMENT DEV
ENV DEBIAN_FRONTEND=noninteractive

# make sure the package repository is up to date
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get install -y ca-certificates
RUN apt-get install -y unzip
RUN apt-get install -y curl
RUN apt-get install -y inetutils-ping
RUN apt-get install -y telnet
# # install editor tool
RUN apt-get -y install less
RUN apt-get -y install vim
RUN apt-get -y install joe
RUN apt-get -y install nano
RUN apt-get -y install git

RUN apt-get update

# install nginx
RUN apt-get -y install nginx

RUN /etc/init.d/nginx start

# customize nginx
ADD config/nginx/nginx.conf /etc/nginx/nginx.conf
ADD config/nginx/sites-available/000-symfony14.example.com.conf /etc/nginx/sites-enabled/000-default_vh.conf
RUN mkdir -p /var/log/nginx/symfony14.example.com
RUN mkdir -p /etc/local_certs/certs
RUN mkdir -p /etc/local_certs/keys

ADD CA_Root/ca_cert/files/certs/example.com/symfony14.example.com.pem /etc/local_certs/certs/ssl_cert.pem
ADD CA_Root/ca_cert/files/nopass/example.com/symfony14.example.com.pem /etc/local_certs/keys/ssl_cert.pem

RUN /etc/init.d/nginx restart

# Basic config

RUN mkdir -p /var/www/webapps
ADD src/core /var/www/webapps/core
RUN chown www-data.www-data /var/www/webapps/ -R
RUN chmod 755 /var/www/webapps/ -R
RUN mkdir -p /var/www/webapps/core/cache
RUN mkdir -p /var/www/webapps/core/log
RUN chmod 777 /var/www/webapps/core/log -R
RUN chmod 777 /var/www/webapps/core/cache -R

# expose http & ssh port
EXPOSE 80
EXPOSE 443

# Custom scripts
ADD config/usr/local/bin/core-entrypoint.sh /usr/local/bin/core-entrypoint.sh
RUN chmod 777 /usr/local/bin/core-entrypoint.sh

ENTRYPOINT core-entrypoint.sh
